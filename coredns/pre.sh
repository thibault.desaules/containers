#!/bin/bash

if grep -qi "WSL2" /proc/version; then
    sed -i -E "s/[0-9]+.[0-9]+.[0-9]+.[0-9]+/$(hostname -I | cut -d ' ' -f1)/g" "/home/tdesaules/Gitlab/containers/coredns/config/desaules.local"
fi